/*!
	 \file
	 \brief Class to operate with errors
	 \author Ilya Andreev
	 \version 1.0
	 \date March 2019
	 \warning Training version, may contain bags and may change in the futures
 */

#ifndef BADWORK_H
#define BADWORK_H


#include <string>
#include <iostream>

class bad_work
{
	std::string errStr_;
	const char * errChr_;
	const char * errFile_;
	const char * errFct_;

	int errLine_;

	bad_work * reason_;

public:
	bad_work(std::string err);
	bad_work(const char * err);
	bad_work(int line);
	bad_work(const char * file, const char * function, int line);
	bad_work(const char * err, const char * file, const char * function, int line);
	bad_work(std::string err, const char * file, const char * function, int line);

	bad_work(const char * file, const char * function, int line, bad_work * reason);
	bad_work(const char * err, const char * file, const char * function, int line, bad_work * reason);
	bad_work(std::string err, const char * file, const char * function, int line, bad_work * reason);

	int what();

	~bad_work();
};

#endif BADWORK_H