/*!
	 \file
	 \brief Function for bad_work class
	 \author Ilya Andreev
	 \version 1.0
	 \date March 2019
	 \warning Training version, may contain bags and may change in the futures
 */

#include "pch.h"
#pragma warning(disable: 4996)
#include "bad_work.h"
#include <direct.h>

FILE * f_log;//! File to logs
static int f_close_fl = 1; //! Flag to file close stats
const char * f_name = "./error/err.log"; //! File path and name

static bool f_open_fl = false; //! Flag to file open status

/*!
	 \brief Open file to logs
	 \param[in] path Path to file
 */
int openFile(const char * path = "error")
{
	f_log = fopen(f_name, "w");
	if (!f_log)
	{
		if (_mkdir(path))
		{
			std::cout << "Fail created directory";
		}

		f_log = fopen(f_name, "w");
	}

	f_open_fl = true;

	return 0;
}

/*!
	 \brief Constuctor from std::string
	 \param[in] err String of error
 */
bad_work::bad_work(std::string err) :
	errStr_(err),
	errChr_(nullptr),
	errFct_(nullptr),
	errFile_(nullptr),
	reason_(nullptr)
{}

/*!
	 \brief Constuctor from string
	 \param[in] err String of error
 */
bad_work::bad_work(const char * err) :
	errChr_(err),
	errFct_(nullptr),
	errFile_(nullptr),
	reason_(nullptr)
{}

/*!
	 \brief Constuctor from line
	 \param[in] err Line of error
 */
bad_work::bad_work(int line) :
	errLine_(line),
	errChr_(nullptr),
	errFct_(nullptr),
	errFile_(nullptr),
	reason_(nullptr)
{
}

/*!
	 \brief Constuctor
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
 */
bad_work::bad_work(const char * file, const char * function, int line) :
	errFile_(file),
	errFct_(function),
	errLine_(line),
	errChr_(nullptr),
	reason_(nullptr)
{
}

/*!
	 \brief Constuctor
	 \param[in] err String of error
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
 */
bad_work::bad_work(const char * err, const char * file, const char * function, int line) :
	errChr_(err),
	errFile_(file),
	errFct_(function),
	errLine_(line),
	reason_(nullptr)
{
}

/*!
	 \brief Constuctor
	 \param[in] err String of error
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
 */
bad_work::bad_work(std::string err, const char * file, const char * function, int line) :
	errStr_(err),
	errChr_(nullptr),
	errFile_(file),
	errFct_(function),
	errLine_(line),
	reason_(nullptr)
{
}
/* LINK */
/*!
	 \brief Constuctor
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
	 \param[in] reason Previous error
 */
bad_work::bad_work(const char * file, const char * function, int line, bad_work * reason) :
	errFile_(file),
	errFct_(function),
	errLine_(line),
	errChr_(nullptr),
	reason_(reason)
{
}

/*!
	 \brief Constuctor
	 \param[in] err String of error
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
	 \param[in] reason Previous error
 */
bad_work::bad_work(const char * err, const char * file, const char * function, int line, bad_work * reason) :
	errChr_(err),
	errFile_(file),
	errFct_(function),
	errLine_(line),
	reason_(reason)
{
}


/*!
	 \brief Constuctor
	 \param[in] err String of error
	 \param[in] file String of file where error happens
	 \param[in] function String of function where error happens
	 \param[in] line Line of error
	 \param[in] reason Previous error
 */
bad_work::bad_work(std::string err, const char * file, const char * function, int line, bad_work * reason) :
	errStr_(err),
	errChr_(nullptr),
	errFile_(file),
	errFct_(function),
	errLine_(line),
	reason_(reason)
{
}

/*!
	 \brief Function for dump the problem
 */
int bad_work::what()
{
	if (!f_open_fl) { openFile(); }

	if (errStr_.length() != 0) {
		fprintf(f_log, "%s    ", errStr_.c_str());
	}
	if (errChr_) {
		fprintf(f_log, "%s    ", errChr_);
	}
	if (errFct_) {
		fprintf(f_log, "in file - {%s}    ", errFile_);
	}
	if (errFile_) {
		fprintf(f_log, "function - (%s)    ", errFile_);
	}
	if (errLine_) {
		fprintf(f_log, "line - [%d]    ", errLine_);
	}

	if (reason_)
	{
		fprintf(f_log, "BECAUSE\n");
		reason_->what();
	}

	if (f_close_fl)
	{
		f_close_fl = fclose(f_log);
		std::cout << "ERROR! To see more info check ./error/err.log\n";

		/*
		char current_work_dir[FILENAME_MAX];

		char command[FILENAME_MAX + 40] = "start ";
		
		_getcwd(current_work_dir, sizeof(current_work_dir));
		strcat(command, current_work_dir);

		strcat(command, "\\error\\err.log");

		std::cout << command << "\n";

		system(command);

		*/

	}

	return 0;
}

/*!
	 \brief Destructor
 */
bad_work::~bad_work()
{
}